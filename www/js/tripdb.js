angular.module('tripdatabases', [])
.factory('TripDB', function($q, Loki) {
  var _db;
  var _triphotels;
  var init = {
  	initDB: function() {            
        var fsAdapter = new LokiCordovaFSAdapter({"prefix": "loki"});  
        _db = new Loki('tripdb',
                {
                    autosave: true,
                    autosaveInterval: 60 * 1000// 1 second
                    //adapter: fsAdapter
                });
    },
    getTripHotel: function() {        
        return $q(function (resolve, reject) {

            var options = {};

            _db.loadDatabase(options, function () {

                _triphotels = _db.getCollection('triphotels');

                if (!_triphotels) {
                    _triphotels = _db.addCollection('triphotels');
                }

                resolve(_triphotels.data);
            });
        });
    },

    addTripHotel: function(triphotel) {
        _articles.insert(triphotel);
    }
  };

  return init;
});