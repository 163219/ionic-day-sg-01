angular.module('shared.createitinerary', ['service.trip'])
.service('ModalCreateItinerary', function($ionicModal, $http, $rootScope, TripService, ModalCompleteItinerary, $model) {
  
  var init = function($scope){
    var promise;
    $parentScope = $scope;
    $scope = $scope || $rootScope.$new();

    $scope.isworking = true;
    $scope.destinations = []; //init

    //load destination from API:
    TripService.loadDestination().then(function(result){
    	if(result != null){
    		//assign to destinations:
    		for(var i = 0; i < result.length; i++){
          var destination = new $model.Destination(result[i]);
  			  $scope.destinations.push(destination);
        }
    	}

    	$scope.isworking = false;
    });

    promise = $ionicModal.fromTemplateUrl('templates/shared/createitinerary.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal){
      $scope.modal = modal;
      return modal;
    });

    $scope.goToDetail = function(name){
      console.log(name);
      ModalCompleteItinerary
      	.init($scope, $parentScope, name)
      	.then(function(modal){
      		modal.show();
      	});
    };

    $scope.closeModal = function() {
      $scope.modal.hide();
    };

    $scope.$on('$destroy', function() {
      $scope.modal.remove();
    });

    return promise;
  }

  return {
    init: init
  }
})
.service('ModalCompleteItinerary', function($ionicModal, $ionicPopup, $http, $rootScope, TripService, ionicDatePicker) {
  
  var init = function($scope, $parentscope, destinationname){
    var promise;
    $mainModalScope = $scope;
    $parentScope = $parentscope;
    $scope = $rootScope.$new();

    //model:
    $scope.trips = {
      destination: destinationname,
      tripName: '',
      startdate: '',
      enddate: ''
    };

    promise = $ionicModal.fromTemplateUrl('templates/shared/completeitinerary.html', {
      scope: $scope,
      animation: 'slide-in-right'
    }).then(function(modal){
      $scope.modal = modal;
      return modal;
    });

    var startDate = {
      callback: function (val) {  //Mandatory
      	var dt = new Date(val);
        $scope.trips.startdate = dt.getDate() + "-" + dt.getMonth() + "-" + dt.getFullYear();
      },
      dateFormat: 'dd-MM-yyyy'
    };

    var endDate = {
      callback: function (val) {  //Mandatory
      	var dt = new Date(val);
        $scope.trips.enddate = dt.getDate() + "-" + dt.getMonth() + "-" + dt.getFullYear();
      },
      dateFormat: 'dd-MM-yyyy'
    };

    $scope.pickStartDate = function(){
    	ionicDatePicker.openDatePicker(startDate);
    };

    $scope.pickEndDate = function(){
    	ionicDatePicker.openDatePicker(endDate);
    };

    $scope.submitTrip = function(){
    	var promise = TripService.postTrip($scope.trips.destination, $scope.trips.tripName, $scope.trips.startdate, $scope.trips.enddate);
    	promise.then(function(result){
    		if(result){
    			$ionicPopup.alert({
             title: 'Trip',
             subTitle: 'New Trip created!'
          });

          //close modal here:
          $scope.modal.hide(); //my modal
          $mainModalScope.closeModal(); //parent modal
    		}else{
    			$ionicPopup.alert({
	               title: 'Trip',
	               subTitle: 'Unable to create new trip!'
	            });
    		}
    	});
    };

    $scope.back = function() {
      $scope.modal.hide();
    };

    $scope.$on('$destroy', function() {
      $scope.modal.remove();
    });

    return promise;
  }

  return {
    init: init
  }
});
